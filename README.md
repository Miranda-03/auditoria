# Auditoria

Proyecto de auditoria (Practicas Profesionalizantes)


https://www.youtube.com/watch?v=HHbIPi43HE4


**Dependencias**:
- python3
- pip
- virtualenv

**Dependencias a instalar con pip**
- psycopg2
- django
- environ
- django-environ
- django-smart-selects
- django_crontab
- django-baton

## Instalación de postgres

### Arch linux 

Instalar el paquete `postgresql`

Cambiar al usuario de postgres `sudo -iu postgres`

Dentro del usuario se inicia el cluster de base de datos `initdb -D /var/lib/postgres/data`

Con `exit` salimos del usuario y iniciamos el servicio `postgresql.service`

Podemos ver el servicio encendido `sudo systemctl status postgresql.service` y nos podemos conectar con Dbeaver al mismo 

Ingresando otra vez al usuario postgres creamos un usuario admin con el siguiente comando: `createuser --interactive` en donde agregamos el nombre del usuario 
admin y ponemos que sea super usuario

Por último creamos la base de datos "prueba" con `createdb prueba` dentro del usuario postgres



### Windows

nos dirigimos a la siguiente página y descargamos la última version:

https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Ejecutamos el instalador y seguimos los pasos que nos indica

Luego entramos a pgAdmin 4 y creamos la base de datos `prueba`


## Aplicación de Django

Una vez clonado al repositorio, en el directorio env/bin/ se introduce el siguiente comando en la consola (powerShell en windows):
`# source activate`
Este comando activa el entorno virtual

Dentro del entorno instale las dependencias con `pip install <nombre>` por ejemplo: `pip install django`

Luego se dirige a la carpeta auditoria/ y para llevar los modelos de la aplicación a la base de datos anteriormente creada introduzca el siguiente comando:
`# python3 manage.py migrate`

Para crear un primer administrador:
`python3 manage.py createsuperuser`
. Luego ingrese un nombre y una contraseña (no hace falta el correo electrónico)


Por último para iniciar el servidor nos quedamos en la misma carpeta y escribimos el comando: 	
`# python3 manage.py runserver`

Ingrese la siguiente url en su navegador y luego el usuario y la contraseña del admin creado anteriormente: 

http://127.0.0.1:8000/admin

## Aclaraciones

Dentro de la aplicación existen 4 tipos de usuario:
.El primero es el admin de la aplicación del cual solo existe uno.
. Luego existen dos tipos de auditores, uno de ellos es el que aprueba las observaciones del otro y ademas tiene permiso de ver todas las obs y todos los informes.

. Auditor 1:

![Alt text](/imgreadme/auditor1.png)

. Auditor 2:

![Alt text](/imgreadme/auditor2.png)

. El cuarto es la persona auditada:

![Alt text](/imgreadme/view.png)


Cada usuario tiene un campo Área, si es un auditor se lo completa con “Auditor” .

Las observaciones cambian de estado automáticamente si se pasan de la fecha establecida 
dependiendo si su estado no es “Finalizado”.

El sistema esta preparado para agregar un e-mail para enviar mails a los auditados y que estos sepan que tienen una observación. Solo hace falta cambiar los datos que estan en auditoria/auditoria/.env . Luego para todos los usuarios auditados que tengan un mail introducido en el sistema se les aplicara esta funcionalidad. 
Aclaración: La EMAIL_HOST_PASSWORD debe ser llenada con una contraseña generada por el proveedor de mail correspondiente.
