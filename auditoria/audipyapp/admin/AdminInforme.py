from copy import deepcopy
from django.contrib import admin
from ..models.Informe import Informe
from django import forms

class verificarNumeroInforme(forms.ModelForm):
    class Meta:
        model = Informe
        fields = "__all__"

    def clean(self):
        super().clean()
        for w in self.cleaned_data.get("NroInforme"):
            if str.isdigit(w) == False:
                if w != "_":
                    raise forms.ValidationError("Número no válido. Ejemplo: 2_2022")

        return self.cleaned_data


class AdminInforme(admin.ModelAdmin):
    form = verificarNumeroInforme
    list_display=("NroInforme", "Tipo",)
    list_filter=("Tipo",)

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = list(self.readonly_fields)
        if not "Usuario" in self.readonly_fields:
            self.readonly_fields.append("Usuario")
        return self.readonly_fields

    def save_model(self, request, obj, form, change):
        obj.Usuario = request.user
        super().save_model(request, obj, form, change)    
    
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser and request.user.has_perm("audipyapp.obs_aud1"):
            queryset = queryset.filter(Usuario=request.user)
        return queryset
    
admin.site.register(Informe, AdminInforme)
