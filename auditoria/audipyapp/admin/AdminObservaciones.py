from audipyapp.models.Area import Area
from audipyapp.models.ComponenteCI import ComponenteCI
from audipyapp.models.DetalleCI import DetalleCI
from audipyapp.models.Estado import Estado
from audipyapp.models.Informe import Informe
from audipyapp.models.ObjetivoControl import ObjetivoControl
from ..models.Observaciones import Observaciones
from django.contrib import admin
from django import forms
from django.contrib.auth import get_user_model as user_model
User = user_model()
from django.core.mail import send_mail
from django.conf import settings



class verificarNumeroRepeticiones(forms.ModelForm):
    class Meta:
        model =  Observaciones
        fields = "__all__"

    def clean(self):
        super().clean()
        numero = self.cleaned_data.get("Repeticiones")
        if numero <= 0:
            raise forms.ValidationError("Cantidad de repeticiones inválida. Debe ser igual o mayor que 1")
        
        start_date = self.cleaned_data.get('PlazoInicial')
        end_date = self.cleaned_data.get('PlazoFinal')

        if end_date < start_date:
            raise forms.ValidationError("La fecha final debe estar adelante de la inicial")
        if end_date == start_date:
            raise forms.ValidationError("La fecha final no puede ser igual a la inicial")
            
        return self.cleaned_data



class AdminObservaciones(admin.ModelAdmin):

    form = verificarNumeroRepeticiones
    filter_horizontal = ("Informes",)

    list_display = ("Hallazgo", "Causa", "Riesgo",)

    list_filter = ("Ponderacion", "Informes__Tipo", "Estado",)
    search_fields = ("Hallazgo", "Riesgo", "Informes__NroInforme",)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "ObjetivoControl":
            kwargs["queryset"] = ObjetivoControl.objects.filter(Activo=True)
        if db_field.name == "componenteci":
            kwargs["queryset"] = ComponenteCI.objects.filter(Activo=True)
        if db_field.name == "DetalleComponenteCI":
            kwargs["queryset"] = DetalleCI.objects.filter(Activo=True)
        if db_field.name == "AreaRespondable":
            kwargs["queryset"] = Area.objects.filter(Activo=True)
        if db_field.name == "Estado":
            kwargs["queryset"] = Estado.objects.filter(Activo=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "Informes" and not request.user.is_superuser:
            kwargs["queryset"] = Informe.objects.filter(Usuario=request.user)
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_fields(self, request, obj=None):
        fields = super(AdminObservaciones, self).get_fields(request, obj)
        if obj:
            fields_to_remove = []
            if not request.user.has_perm("audipyapp.obs_aud2"):
                fields_to_remove.append('Aprobado')
            else:
                fields_to_remove = ['todos', ]
            for field in fields_to_remove:
                if field in fields:
                    fields.remove(field)
        return fields

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = list(self.readonly_fields)
        if not "Usuario" in self.readonly_fields:
            self.readonly_fields.append("Usuario")
        if not "Fecha" in self.readonly_fields:
            self.readonly_fields.append("Fecha")
        readonly = (
            'OpinionAuditado',
            'PlanAccion',

        )
        readonly_for_viewer = (
            'ObjetivoControl',
            'componenteci',
            'DetalleComponenteCI',
            'Causa',
            'Hallazgo',
            'Riesgo',
            'Ponderacion',
            'Sustentable',
            'AreaRespondable',
            'Informes',
            'Referencia',
            'Recomendacion',
            'Estado',
            'Repeticiones',
        )

        if not request.user.has_perm("audipyapp.obs_view") or request.user.is_superuser:
            for field in readonly:
                if not field in self.readonly_fields:
                    self.readonly_fields.append(field)
            for field2 in readonly_for_viewer:
                if field2 in self.readonly_fields:
                    self.readonly_fields.remove(field2)
        else:
            for field in readonly_for_viewer:
                if not field in self.readonly_fields:
                    self.readonly_fields.append(field)
            for field2 in readonly:
                if field2 in self.readonly_fields:
                    self.readonly_fields.remove(field2)

        return self.readonly_fields

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser and request.user.has_perm("audipyapp.obs_aud1"):
            queryset = queryset.filter(Usuario=request.user)
        if not request.user.is_superuser and request.user.has_perm("audipyapp.obs_view"):
            queryset = queryset.filter(AreaRespondable=request.user.getArea())
        return queryset

    def save_model(self, request, obj, form, change):
        obj.Usuario = request.user

        def sendEmail(user):
            send_mail('A cool subject', 'A stunning message', settings.EMAIL_HOST_USER, [user.email])

        usersArea = User.objects.filter(Area=obj.AreaRespondable)
        usersArea = list(usersArea)
       
        for item in usersArea:
            sendEmail(item)

        super().save_model(request, obj, form, change)


admin.site.register(Observaciones, AdminObservaciones)
