from django.contrib import admin

# Register your models here.

from ..models.ComponenteCI import ComponenteCI
from ..models.Observaciones import Observaciones
from ..models.DetalleCI import DetalleCI
from ..models.Area import Area
from ..models.TiposInforme import TiposInforme
from ..models.ObjetivoControl import ObjetivoControl
from ..models.Estado import Estado

admin.site.register(DetalleCI)
admin.site.register(TiposInforme)
admin.site.register(ComponenteCI)
admin.site.register(Area)
admin.site.register(ObjetivoControl)
admin.site.register(Estado)

