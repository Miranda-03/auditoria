
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from ..models.UsuarioArea import UsuarioArea

class CustomUserAdmin(UserAdmin):
     list_display = (
        'username', 'first_name', 'last_name', 'Area',
        )

     fieldsets = (
        (None, {
            'fields': ('username', 'password', 'Area')
        }),
        ('Información Personal', {
            'fields': ('first_name', 'last_name', 'email')
        }),
        ('Permisos', {
            'fields': (
                'is_active', 'is_staff', 'is_superuser',
                'groups', 'user_permissions'
                )
        }),
        ('Fechas importantes', {
            'fields': ('last_login', 'date_joined')
        }),
        )


admin.site.register(UsuarioArea, CustomUserAdmin)