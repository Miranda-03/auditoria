from django.apps import AppConfig


class AudipyappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'audipyapp'
