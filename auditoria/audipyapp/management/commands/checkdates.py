from django.core.management.base import BaseCommand
from django.utils import timezone
from ...models.Observaciones import Observaciones
from ...models.Estado import Estado
import datetime

class Command(BaseCommand):
    help = 'Cambia el estado segun la fecha'

    def handle(self, *args, **kwargs):
        now = datetime.date.today()
        estados = Estado._meta.model.objects.all()
        queryset = Observaciones._meta.model.objects.all().filter(Estado=2)
        for obj in queryset:
            Plazo_final = getattr(obj, 'PlazoFinal')
            if now > Plazo_final:
                opinion = getattr(obj, 'OpinionAuditado')
                if opinion == None:
                    obj.Estado = estados[0]
                    obj.save() 
                else:
                    obj.Estado = estados[1]
                    obj.save()