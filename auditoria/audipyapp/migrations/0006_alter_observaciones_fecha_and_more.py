# Generated by Django 4.1 on 2022-09-22 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('audipyapp', '0005_informe_usuario'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observaciones',
            name='Fecha',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='observaciones',
            name='Ponderacion',
            field=models.CharField(choices=[('BAJA', 'Baja'), ('MEDIA', 'Media'), ('ALTA', 'Alta')], max_length=6, verbose_name='nivel de ponderación'),
        ),
    ]
