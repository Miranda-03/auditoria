# Generated by Django 4.1 on 2022-09-22 18:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('audipyapp', '0006_alter_observaciones_fecha_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observaciones',
            name='Ponderacion',
            field=models.CharField(choices=[('MEDIA', 'Media'), ('ALTA', 'Alta'), ('BAJA', 'Baja')], max_length=6, verbose_name='nivel de ponderación'),
        ),
    ]
