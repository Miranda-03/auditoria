# Generated by Django 4.1 on 2022-10-04 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('audipyapp', '0007_alter_observaciones_ponderacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observaciones',
            name='Ponderacion',
            field=models.CharField(choices=[('BAJA', 'Baja'), ('MEDIA', 'Media'), ('ALTA', 'Alta')], max_length=6, verbose_name='nivel de ponderación'),
        ),
    ]
