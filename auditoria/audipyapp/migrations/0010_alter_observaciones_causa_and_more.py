# Generated by Django 4.0 on 2022-10-27 17:41

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('audipyapp', '0009_alter_observaciones_ponderacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observaciones',
            name='Causa',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(max_length=80), size=None),
        ),
        migrations.AlterField(
            model_name='observaciones',
            name='Informes',
            field=models.ManyToManyField(to='audipyapp.Informe'),
        ),
        migrations.AlterField(
            model_name='observaciones',
            name='Ponderacion',
            field=models.CharField(choices=[('ALTA', 'Alta'), ('MEDIA', 'Media'), ('BAJA', 'Baja')], max_length=6, verbose_name='nivel de ponderación'),
        ),
        migrations.AlterField(
            model_name='usuarioarea',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AlterField(
            model_name='usuarioarea',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
    ]
