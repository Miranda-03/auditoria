from tabnanny import verbose
from .Desplegable import Desplegable

class Area(Desplegable):
    class Meta: 
        verbose_name = "Area"
        verbose_name_plural = "Areas"
    pass