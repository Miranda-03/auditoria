from .Desplegable import Desplegable


class ComponenteCI(Desplegable):
  
    class Meta:
        verbose_name = "Componente de control interno"
        verbose_name_plural = "Componentes de control interno"
        
    pass

    
