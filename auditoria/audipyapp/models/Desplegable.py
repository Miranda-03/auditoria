from django.db import models

class Desplegable(models.Model):

    class Meta:
        abstract = True

    Clave = models.CharField(max_length=80)
    Valor = models.CharField(max_length=80)
    Activo= models.BooleanField(default=True)

    def __str__(self):
        return self.Valor