from django.db import models

from .Desplegable import Desplegable
from .ComponenteCI import ComponenteCI


class DetalleCI(Desplegable):

    class Meta:
        verbose_name = "Detalle de control interno"
        verbose_name_plural = "Detalles de control interno"

    componenteci = models.ForeignKey(ComponenteCI, on_delete=models.CASCADE,  null=True, blank=True, verbose_name="Detalle de control interno")