from tabnanny import verbose
from .Desplegable import Desplegable

class Estado(Desplegable):
    class Meta:
        verbose_name = "Estado"
        verbose_name_plural = "Estados"
    pass
