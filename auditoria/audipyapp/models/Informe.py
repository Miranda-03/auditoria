
from distutils import extension
from django.db import models
from django.forms import UUIDField
from .TiposInforme import TiposInforme
from uuid import uuid4
import re
from time import gmtime, strftime
#from django.contrib.auth import get_user_model as user_model
from .UsuarioArea import UsuarioArea    
#User = user_model()

class Informe(models.Model):
    class Meta:
        verbose_name = (' Informe')
        verbose_name_plural = (' Informes')

    id=models.UUIDField(primary_key=True,default=uuid4,unique=True,  editable=False)

    Usuario = models.ForeignKey(UsuarioArea, on_delete=models.SET_NULL, null=True)

    NroInforme = models.CharField(max_length=10)

    def nombre_archivo(instance, filename):
        showtime = strftime("%Y_%m_%d__%H:%M:%S", gmtime())
        if "." in filename:
            ext = re.search('[.](.+)', filename).group(1)
            ext = "." + ext
            return 'Informes/' + showtime + ext
        return filename

    Directorio= models.FileField(upload_to=nombre_archivo,  blank=True, null=True)

    Tipo = models.ForeignKey(TiposInforme, on_delete=models.SET_NULL, null=True, verbose_name="tipo de informe")

    def __str__(self):
        return self.NroInforme