from tabnanny import verbose
from .Desplegable import Desplegable

class ObjetivoControl(Desplegable):
    class Meta:
        verbose_name = "Objetivo de control"
        verbose_name_plural = "Objetivos de control"
    pass