from tabnanny import verbose
from django.contrib.postgres.fields import ArrayField
from django.db import models
from .ComponenteCI import ComponenteCI
from .Estado import Estado
from .Area import Area
from .Informe import Informe
from .ObjetivoControl import ObjetivoControl
from .DetalleCI import DetalleCI
from smart_selects.db_fields import ChainedForeignKey
import datetime
from django.contrib.auth import get_user_model as user_model
User = user_model()
#  Create your models here.
 
CATEGORY = {
    ('ALTA', 'Alta'),
    ('MEDIA', 'Media'),
    ('BAJA', 'Baja'),
}


class Observaciones(models.Model):

    class Meta:
        verbose_name = "Observacion"
        verbose_name_plural = "Observaciones"
        permissions = (
            ("obs_aud2", "Auditor 2 permisos"),
            ("obs_aud1", "Auditor 1 permisos"),
            ("obs_view", "Auditor view area"),
        )   
    # agustin: 123HolaMundo
    # martin: 03M1fauditoria
    # santiago: 01MSn123
    # ignacio: 2022holaMundo
    # gary: 123
    Usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    ObjetivoControl = models.ForeignKey(ObjetivoControl, on_delete=models.SET_NULL, null=True, verbose_name="Objetivo de Control")

    componenteci = models.ForeignKey(ComponenteCI,
                                     on_delete=models.SET_NULL,
                                     null=True,
                                     verbose_name="Componente de control interno",
                                     )

    #DetalleComponenteCI = models.ForeignKey(DetalleCI, on_delete=models.SET_NULL, null=True, verbose_name="Detalle de control interno")

    #DetalleComponenteCI = models.CharField(max_length=50, blank=True)

    #DetalleComponenteCI =  property(filtrarDropdown(ComponenteCI))

    DetalleComponenteCI = ChainedForeignKey(
        DetalleCI,
        chained_field="componenteci",
        chained_model_field="componenteci",
        show_all=False,
        auto_choose=True,
        sort=True,
        verbose_name="Detalle del componente"
    )

    Causa = models.TextField()

    Hallazgo = models.TextField()

    Riesgo = models.TextField()

    Ponderacion = models.CharField(choices=CATEGORY, max_length=6, verbose_name="nivel de ponderación")

    Sustentable = models.BooleanField() 

    AreaRespondable = models.ForeignKey(Area, on_delete=models.SET_NULL, null=True, verbose_name="Area responsable")

    Informes = models.ManyToManyField(Informe)

    Referencia = models.CharField(max_length=80)
    Recomendacion = models.TextField()
    OpinionAuditado = models.TextField()
    PlanAccion = models.TextField(("Plan de accion"))

    PlazoInicial = models.DateField(("Desde"), default=datetime.date.today)

    PlazoFinal = models.DateField(("Hasta"))

    Estado = models.ForeignKey(Estado, on_delete=models.SET_NULL, null=True, verbose_name="estado")

    Fecha = models.DateTimeField(auto_now_add=True, blank=True)

    Repeticiones = models.IntegerField()
    
    Aprobado = models.BooleanField(default=False)