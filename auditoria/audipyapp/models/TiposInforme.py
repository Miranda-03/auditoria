from tabnanny import verbose
from .Desplegable import Desplegable

class TiposInforme(Desplegable):
    class Meta:
        verbose_name = "Tipo de informe"
        verbose_name_plural = "Tipos de informe"
    pass            