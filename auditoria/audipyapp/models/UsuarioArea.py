from django.contrib.auth.models import AbstractUser
from django.db import models
from .Area import Area

class UsuarioArea(AbstractUser):
    Area = models.ForeignKey(Area, on_delete=models.SET_NULL, null=True)

    def getArea(self):  
        return self.Area