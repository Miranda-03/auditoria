from django.db import models
from django.contrib.auth import get_user_model as user_model
User = user_model()

class notification(models.Model):
    class Meta:
        verbose_name = ('notificación')
        verbose_name_plural = ('notificaciones')
    
    Usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    Descripcion = models.CharField(max_length=50)
